# Wizarding World Quotes

> WWQuotes est une API qui permet de générer des citations du monde de Harry Potter. 

Dans ce projet vous trouverez d'une part l'API et d'une autre une interface web qui l'utilise.  
Bien sur les deux sont indépendants l'un de l'autre.  

L'API utilise expressJS, Sequelize et SQLite3. 
L'interface web dédié utilise ReactJS, Bootstrap et Axios. 

### Installation de l'API WWQuotes :

* Ce placer dans le dossier node_backend 
    `cd node_backend` 
* Installer les pacquets npm 
    `npm install` 
* Lancer le serveur de développement (sur le port 3000 de préférence) 
    `npm run dev` 


### Documentation de l'api :

* Créer une citation (POST) -> `localhost:3000/quote/create` 
* Supprimer une citation (POST) -> `localhost:3000/quote/delete` 
* Mettre à jour une citation (POST) -> `localhost:3000/quote/update/:id` 
* Récuperer une citation (GET) -> `localhost:3000/quote/list` 
* Récuperer toutes les citations (GET) -> `localhost:3000/quote/get/:id` 
* Récuperer aléatoirement une citation (GET) -> `localhost:3000/quote/random`  


### Utilisation de l'interface web (optionel): 

* Ce placer dans le dossier `appnode`:   
    `cd appnode` 
* Installer l'application  
    `yarn install` 
* Lancer l'application (autre que le port 3000, par exemple 3001):  
    `yarn start` 





---------------------------------------------------


## Doc React Générée
This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `yarn eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: https://facebook.github.io/create-react-app/docs/code-splitting

### Analyzing the Bundle Size

This section has moved here: https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size

### Making a Progressive Web App

This section has moved here: https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app

### Advanced Configuration

This section has moved here: https://facebook.github.io/create-react-app/docs/advanced-configuration

### Deployment

This section has moved here: https://facebook.github.io/create-react-app/docs/deployment

### `yarn build` fails to minify

This section has moved here: https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify
