const express = require("express");
const router = express.Router();
const employeeController = require("../controllers/employeeController");

router.post('/delete',employeeController.delete);
router.post('/update/:id', employeeController.update);
router.get("/list", employeeController.list);
router.post('/create',employeeController.create);
router.get("/get/:id", employeeController.get);

// router.get("/testdata", employeeController.testData);
// router.get("/save", (req, res) => {
//   res.json({ status: "Employee Saved" });
// });

module.exports = router;
