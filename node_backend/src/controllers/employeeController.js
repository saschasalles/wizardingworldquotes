const controllers = {};
const role = require("../model/role");
const sequelize = require("../model/database");
const employee = require("../model/employee");

sequelize.sync();

controllers.delete = async (req, res) => {
  const { id } = req.body;
  const del = await employee.destroy({
    where: { id: id },
  });
  res.json({ success: true, deleted: del, message: "Deleted successful" });
};

controllers.update = async (req, res) => {
  const { id } = req.params;
  const { name, email, address, phone, role } = req.body;
  data = await employee
    .update(
      {
        name: name,
        email: email,
        address: address,
        phone: phone,
        role: role,
      },
      {
        where: { id: id },
        returning: true,
        plain: true,
      }
    )
    .then(() => employee.save())
    .then(function (data) {
      return data;
    })
    .catch((error) => {
      return error;
    });

  console.log(data.toString());
  res.status(200).json({
    success: true,
    message: "Employé correctement modifié!",
    data: data,
  });
};

controllers.create = async (req, res) => {
  // data
  const { name, email, address, phone, role } = req.body;
  // create
  const data = await employee
    .create({
      name: name,
      email: email,
      address: address,
      phone: phone,
      roleId: role,
    })
    .then((data) => data)
    .catch((error) => {
      console.log("Error " + error);
      return error;
    });

  res.status(200).json({
    success: true,
    message: "Employé bien ajouté !",
    data: data,
  });
};

controllers.get = async (req, res) => {
  const { id } = req.params;
  const data = await employee
    .findAll({
      where: { id: id },
      include: [role],
    })
    .then((data) => data)
    .catch((error) => console.log(error));
  res.json({ success: true, data: data });
};

controllers.list = async (req, res) => {
  const data = await employee
    .findAll({
      include: [role],
    })
    .then((data) => data)
    .catch((error) => console.log(error));

  res.json({ success: true, data: data });
};

// controllers.testData = async (req, res) => {
//   const response = await sequelize
//     .sync()
//     .then(function () {
//       role.create({
//         role: "Admin",
//       });

//       // create employee
//       employee.create({
//         name: "Malena Morgan",
//         email: "malena@mail.com",
//         address: "California Cll 108",
//         phone: "123456789",
//         roleId: 1,
//       });
//       const data = employee.findAll();
//       return data;
//     })
//     .catch((err) => console.log(err));

//   res.json({ success: true, data: response });
// };

// controllers.test = (req, res) => {
//   const data = {
//     name: "John Smith",
//     age: 20,
//     city: "London",
//   };

//   console.log("Send data from controller employee");
//   res.json(data);
// };
module.exports = controllers;
