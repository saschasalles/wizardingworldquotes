let Sequelize = require("sequelize");

const sequelize = new Sequelize(
{
  storage: './database.db',
  dialect: "sqlite",
});

module.exports = sequelize;
