let Sequelize = require('sequelize');
let sequelize = require('./database');


let nameTable = "role";

let role = sequelize.define(
  nameTable,
  {
    role: Sequelize.STRING,
  },
  {
    timestamp: false,
  }
);

module.exports = role;