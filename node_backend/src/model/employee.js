let Sequelize = require('sequelize');
let sequelize = require('./database');
let role = require("./role");
let nameTable = "employee";

let employee = sequelize.define(nameTable, {
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true,
  },
  name: Sequelize.STRING,
  email: Sequelize.STRING,
  address: Sequelize.STRING,
  phone: Sequelize.STRING,
  roleId: {
    type: Sequelize.INTEGER,
    references: {
      model: role,
      key: 'id'
    }
  },
  
});


employee.belongsTo(role);

module.exports = employee
